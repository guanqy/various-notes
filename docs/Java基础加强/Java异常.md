## Java中的异常
#### 异常体系
```text
Throwable 是 java 语言中所有错误和异常的超类（万物即可抛）。
它有两个子类：Error和Exception
其中 Error 为错误，是程序无法处理的，如 OutOfMemoryError、ThreadDeath 等，出现这种情况你唯一能做的就是听之任之，交由 JVM 来处理，不过 JVM 在大多数情况下会选择终止线程。
Exception是可以处理的：它又分为两种 CheckedException（受捡异常），一种是 UncheckedException（不受检异常）。
```
+ CheckedException：发生在编译期，，必须要使用 try…catch（或者throws）否则编译不通过
+ UncheckedException：发生在运行期，具有不确定性，主要是由于程序的逻辑问题所引起的，难以排查，我们一般都需要纵观全局才能够发现这类的异常错误，所以在程序设计中我们需要认真考虑，
好好写代码，尽量处理异常，即使产生了异常，也能尽量保证程序朝着有利方向发展。

所以：对于可恢复的条件使用被检查的异常（CheckedException），对于程序错误（言外之意不可恢复，大错已经酿成）使用运行时异常（RuntimeException）。

#### 异常使用
```java
public class ExceptionTest {
public static void main(String[] args) {
    String file = "D:\\exceptionTest.txt";
    FileReader reader;
    try {
        reader = new FileReader(file);
        Scanner in = new Scanner(reader);
        String string = in.next();
    System.out.println(string + "不知道我有幸能够执行到不.....");
    } catch (FileNotFoundException e) {
        e.printStackTrace();
        System.out.println("对不起,你执行不到...");
    }
    finally{
        System.out.println("finally 在执行...");
    } 
}
}
```
这是段非常简单的程序，用于读取 D 盘目录下的 exceptionText.txt 文件，同时读取其中的内容、输出。首先 D
盘没有该文件，运行程序结果如下：
```text
java.io.FileNotFoundException: D:\exceptionTest.txt (系统找不到指定的文件。)
at java.io.FileInputStream.open(Native Method)
at java.io.FileInputStream.<init>(FileInputStream.java:106)
at java.io.FileInputStream.<init>(FileInputStream.java:66)
at java.io.FileReader.<init>(FileReader.java:41)
at com.test9.ExceptionTest.main(ExceptionTest.java:19)
对不起,你执行不到...
finally 在执行...
```
```java
public class ExceptionTest {
    public static void main(String[] args) {
        int[] a = {1,2,3,4};
        System.out.println(a[4]);
        System.out.println("我执行了吗???");
    } 
}
//运行结果
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 4
at com.test9.ExceptionTest.main(ExceptionTest.java:14)
```
```text
java.io.FileNotFoundException: D:\exceptionTest.txt (系统找不到指定的文件。)

Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 4
```
俩个异常可以得到一个问题：若程序中显示的声明了某个异常，则抛出异常时不会显示出
处，若程序中没有显示的声明某个异常，当抛出异常时，系统会显示异常的出处。

#### 自定义异常类
Java 确实给我们提供了非常多的异常，但是异常体系是不可能预见所有的希望加以报告的错误，所以 Java 允许
我们自定义异常来表现程序中可能会遇到的特定问题，总之就是一句话：我们不必拘泥于 Java 中已有的异常类
型。
###### Java自定义异常的使用要经历如下四个步骤
1、定义一个类继承 Throwable 或其子类。
2、添加构造方法(当然也可以不用添加，使用默认构造方法)。
3、在某个方法类抛出该异常。
4、捕捉该异常。
```java
/** 自定义异常 继承Exception类 **/
public class MyException extends Exception{
    public MyException(){
    }
    public MyException(String message){
        super(message);
    } 
}
    public class Test {
        public void display(int i) throws MyException{
    if(i == 0){
        throw new MyException("该值不能为0.......");
    }
    else{
        System.out.println( i / 2);
    } }
    public static void main(String[] args) {
        Test test = new Test();
        try {
            test.display(0);
            System.out.println ("---------------------");
        } catch (MyException e) {
            e.printStackTrace();
        }
    } 
}
```
#### 异常链
在设计模式中有一个叫做责任链模式，该模式是将多个对象链接成一条链，客户端的请求沿着这条链传递直到被
接收、处理。同样 Java 异常机制也提供了这样一条链：异常链。我们知道每遇到一个异常信息，我们都需要进行 try…catch,一个还好，如果出现多个异常呢？分类处理肯定会比
较麻烦，那就一个 Exception 解决所有的异常吧。这样确实是可以，但是这样处理势必会导致后面的维护难度增
加。最好的办法就是将这些异常信息封装，然后捕获我们的封装类即可。
诚然在应用程序中，我们有时候不仅仅只需要封装异常，更需要传递。怎么传递？throws!！binge，正确！！但
是如果仅仅只用 throws 抛出异常，那么你的封装类，怎么办？？
我们有两种方式处理异常，一是 throws 抛出交给上级处理，二是 try…catch 做具体处理。但是这个与上面有什
么关联呢？try…catch 的 catch 块我们可以不需要做任何处理，仅仅只用 throw 这个关键字将我们封装异常信
息主动抛出来。然后在通过关键字 throws 继续抛出该方法异常。它的上层也可以做这样的处理，以此类推就会产生一条由异常构成的异常链。
通过使用异常链，我们可以提高代码的可理解性、系统的可维护性和友好性。
同理，我们有时候在捕获一个异常后抛出另一个异常信息，并且希望将原始的异常信息也保持起来，这个时候也需要使用异常链。
在异常链的使用中，throw 抛出的是一个新的异常信息，这样势必会导致原有的异常信息丢失，如何保持？在 Th
rowable 及其子类中的构造器中都可以接受一个 cause 参数，该参数保存了原有的异常信息，通过 getCause
()就可以获取该原始异常信息。
```java
public void test() throws XxxException{
    try {
        //do something:可能抛出异常信息的代码块
    } catch (Exception e) {
        throw new XxxException(e);
    } 
}
```
```java
public class Test {
    public void f() throws MyException{
    try {
    FileReader reader = new FileReader("G:\\myfile\\struts.txt");
    Scanner in = new Scanner(reader);
    System.out.println(in.next());
    } catch (FileNotFoundException e) {
    //e 保存异常信息
    throw new MyException("文件没有找到--01",e);
    } }
    public void g() throws MyException{
    try {
    f();
    } catch (MyException e) {
    //e 保存异常信息
    throw new MyException("文件没有找到--02",e);
    } }
    public static void main(String[] args) {
    Test t = new Test();
    try {
    t.g();
    } catch (MyException e) {
    e.printStackTrace();
    } }
}
```
运行结果：
```text
com.test9.MyException: 文件没有找到--02
at com.test9.Test.g(Test.java:31)
at com.test9.Test.main(Test.java:38)
Caused by: com.test9.MyException: 文件没有找到--01
at com.test9.Test.f(Test.java:22)
at com.test9.Test.g(Test.java:28)
... 1 more
Caused by: java.io.FileNotFoundException: G:\myfile \struts.txt (系统找不到指定的路径。)
at java.io.FileInputStream.open(Native Method)
at java.io.FileInputStream.<init>(FileInputStream.java:106)
at java.io.FileInputStream.<init>(FileInputStream.java:66)
at java.io.FileReader.<init>(FileReader.java:41)
at com.test9.Test.f(Test.java:17)
... 2 more
```
如果在程序中,去掉 e，也就是：throw new MyException(“文件没有找到 –02″);
那么异常信息就保存不了，运行结果如下：
```text
com.test9.MyException: 文件没有找到--02
at com.test9.Test.g(Test.java:31)
at com.test9.Test.main(Test.java:38)
```
#### 异常的使用误区
1、尽可能的减小try块
2、保证所有资源都被正确的释放。充分利用finally关键词
3、catch 语句应当尽量指定具体的异常类型，而不应该指定涵盖范围太广的 Exception 类。 不要一个 Exception 试图处理所有可能出现的异常。
4、捕获异常不处理，（改进的方法）
+ 处理异常。对所发生的的异常进行一番处理，如修正错误、提醒。再次申明 ex.printStackTrace() 算不上已经“处理好了异常”.  
+ 重新抛出异常。既然你认为你没有能力处理该异常，那么你就尽情向上抛吧！！！
+ 封装异常。这是 LZ 认为最好的处理方法，对异常信息进行分类，然后进行封装处理。
+ 不要捕获异常。
5、异常信息不明确：最好可以提供一些文字信息，需要讲一些异常类的信息所在的类、方法、何种异常都需要记录在日志文件中
6、既然捕获了异常，就要对它进行适当的处理。不要捕获异常之后又把它丢弃，不予理睬。 不要做一个不负责的人。
7、在异常处理模块中提供适量的错误原因信息，组织错误信息使其易于理解和阅读。
8、不要在finally块中处理返回值
9、不要在构造函数中抛出异常。

#### try…catch、throw、throws
throws：throws 是方法抛出异常。在方法声明中，如果添加了 throws 子句，表示该方法即将抛出异常，异常的处理交由
它的调用者，至于调用者任何处理则不是它的责任范围内的了。所以如果一个方法会有异常发生时，但是又不想
处理或者没有能力处理，就使用 throws 吧！
throw：它不可以单独使用，要么与 try…catch 配套使用，要么与 throws 配套使用。
```java
//使用throws抛出异常
public void f() throws MyException{
try {
FileReader reader = new FileReader("G:\\myfile\\struts.txt");
Scanner in = new Scanner(reader);
System.out.println(in.next());
} catch (FileNotFoundException e) {
throw new MyException("文件没有找到", e); //throw
} }
```